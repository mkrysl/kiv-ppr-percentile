\documentclass[11pt]{article}

\usepackage[a4paper]{geometry}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[czech]{babel}
\usepackage{graphicx}
\usepackage{fancyvrb}
\usepackage{listings}
\usepackage{float}
\usepackage{enumitem}
\usepackage{mathtools}
\usepackage{amsmath}

\setlength{\parindent}{0em}
\setlength{\parskip}{1em}

\begin{document}
    \begin{titlepage}
        \begin{center}
            \includegraphics[width=0.6\textwidth]{img/fav_logo}

            \vspace{1cm}

            \LARGE{Semestrální práce}

            \LARGE{z~předmětu}

            \vspace{0.5cm}

            \Huge{KIV/PPR}

            \vspace{1.5cm}

            \LARGE{Program pro přečtení souboru a nalezení zadaného percentilu}

            \vspace{3cm}

            \Large{\textbf{Miroslav Krýsl}}

            \vspace{0.5cm}

            \today

            \vfill
            \includegraphics[width=0.4\textwidth]{img/kiv_logo}

        \end{center}
    \end{titlepage}


    \section{Zadání}

    Program semestrální práce dostane jako jeden z~parametrů cestu k~souboru, který je přístupný pouze pro čtení.
    Bude ho interpretovat jako čísla v~plovoucí řádové čárce -- 64-bitový double.
    Program najde číslo na arbitrárně zadaném percentilu, dalším z~parametrů, a vypíše první a poslední pozici v~souboru, na které se toto číslo nachází.

    Program se bude spouštět následovně:

    \begin{lstlisting}
pprsolver.exe soubor percentil procesor
    \end{lstlisting}

    \begin{itemize}
        \item soubor - cesta k~souboru, může být relativní k~program.exe, ale i absolutní
        \item percentil - číslo 1--100
        \item procesor - řetězec určující, na jakém procesoru a jak výpočet proběhne:
        \begin{description}
            \item[single] - jednovláknový výpočet na CPU
            \item[SMP] - vícevláknový výpočet na CPU
            \item[název OpenCL zařízení] - pozor, v~systému může být několik OpenCL platforem
        \end{description}
    \end{itemize}

    \noindent Další požadavky:

    \begin{itemize}[label={--}]
        \item Součástí programu bude watchdog vlákno, které bude hlídat správnou funkci programu.
        \item Testovaný soubor bude velký několik GB, ale paměť bude omezená na 250 MB. To zařídí validátor.
        \item Program musí skončit do 15 minut na iCore7 Skylake.
        \item Program nebude mít povoleno vytvářet soubory na disku.
        \item Jako čísla budete uvažovat pouze ty 8-bytové sekvence, pro které \texttt{std::fpclassify} vrátí \texttt{FP\_NORMAL} nebo \texttt{FP\_ZERO}. Jiné sekvence budete ignorovat.
        \item Všechny nuly jsou si stejně rovné.
        \item Pozice v~souboru vypisujte v~bytech, tj. indexováno od nuly.
        \item Nalezené číslo číslo vypište v~hexadecimálním formátu, např. pomocí \texttt{std::hexfloat}.
    \end{itemize}



    \section{Úvod}

    Cílem této semestrální práce je vytvořit program pro nalezení hodnoty zadaného percentilu ze souboru a také pozici jejího prvního a posledního výskytu.
    Prostředí pro výpočet má však omezené zdroje, tj. maximální velikost dostupné paměti a také maximální dobu běhu výpočtu.
    Cílem také to, aby byl program schopen operovat třemi různými způsoby, tj. sériově, paralelně s~použitím SMP a parallelně použitím knihovny OpenCL.

    Je tedy nutné navrhnout a implementovat takový algoritmus, kterému stačí omezené množství paměti, je efektivní = rychlý, a také alespoň z~části paralelizovatelný.

    \section{Algoritmus}

    Jedna z~definic percentilu, kterou jsem použil pro tuto práci, je následující.

    P-tý percentil $(1 \leq P \leq 100 )$ množiny $N$ seřazených hodnot je nejmenší hodnota z~této množiny taková, že ne více než P procent všech hodnot z~množiny je menší než tato hodnota a alespoň P procent hodnot je menší nebo rovno tété hodnotě.
    Index této hodnoty v~seřazené množine lze vypočítat pomocí následující rovnice \eqref{eq:percentile}.

    \begin{equation}\label{eq:percentile}
        i = \left\lceil \frac{P}{100} \cdot N \right\rceil
    \end{equation}

    Algoritmus pro nalezení čísla ze souboru na zadaném percentilu je jednoduchý.
    Stačí načíst všechna čísla, vyfiltrovat nevalidní čísla, spočítat index $i$ percentilu $P$ podle rovnice \ref{eq:percentile} a získat $i$-té nejmenší číslo.
    Problémem je však omezená velikost dostupné operační paměti.

    Můj algoritmus toto řeší tak, že během několika průchodů souborem určí pomocí půlení intervalu konečný interval čísel $\left(lower, upper\right)$ takový,
    že se zadaný percentil nachází uvnitř tohoto intervalu a že počet čísel ze souboru náležících do tohoto intervalu se vejde do poskytnuté operační paměti.

    Před každým průchodem se určí spodní a horní mez intervalu a také náhodně zvolený pivot náležící do tohoto intervalu.
    Při průchodu se spočítá, kolik čísel z~intervalu je menších, větších nebo rovno pivotu.
    Podle toho, do kterého podintervalu spadá spočtený index $i$ percentilu se upraví meze (interval se zmenší) a také index percentilu v~rámci intervalu.

    Po nalezení konečného intervalu se všechna čísla z~tohoto intervalu načtou do paměti a najde se $i$-té nejmenší číslo.
    Nakonec stačí ještě jednou projít soubor a zjistit pozici prvního a posledního výskytu nalezeného percentilu.

    Detailní postup algoritmu je na diagramech \ref{fig:alg1} a \ref{fig:alg2}. Data-flow diagram postupu, jakým se zpracovávají načtená čísla ze souboru, je
    na obrázku \ref{fig:dataflow}.

    \begin{figure}[H]
        \centering
        \includegraphics[width=0.8\textwidth]{img/ppr_alg_1}
        \caption{Algoritmus - přehled hlavních fází}
        \label{fig:alg1}
    \end{figure}

    \pagebreak

    \begin{figure}[H]
        \centering
        \includegraphics[width=0.8\textwidth]{img/ppr_alg_2}
        \caption{Algoritmus - fáze algoritmu pro hledání intervalu}
        \label{fig:alg2}
    \end{figure}

    \pagebreak

    \begin{figure}[H]
        \centering
        \includegraphics[width=\textwidth]{img/data_flow}
        \caption{Algoritmus - data flow postupu načítání a zpracování čísel ze souboru}
        \label{fig:dataflow}
    \end{figure}

    \pagebreak


    \section{Implementace}

    Program je naimplementován v~jazyce C++17. Pro paralelní verze jsou použity knihovny Intel TBB a OpenCL.

    Hlavní vstupní bod programu -- funkce \texttt{main} naparsuje argumenty převzaté z~příkazové řádky
    a spustí podle nich výpočet, který provede tzv. solver, což je abstraktní třída. Každý konkrétní solver
    poté implementuje logiku pro počítání čísel v~podintervalech, hledání n-tého nejmenšího prvku z~konečného intervalu
    a také hledání první a poslední pozice výskytu čísla v~souboru.


    Spolu s~každým solverem je spouštěn watchdog, který kontroluje, zda se výpočet nezasekl a zda velikost hledaného
    intervalu konverguje k~menšímu.

    Soubor je vždy načítán po částech, a po částech také zpracováván.

    Detailní popis struktur a komentáře k~algoritmům jsou popsání ve zdrojovém kódu.

    \section{Paralelizace a optimalizace}

    Čtení dat ze souboru nemá z~principu smysl dělat paralelně, v~případě čtení z~pevného disku
    by mohlo naopak dojít ke značnému zpomalení. Co naopak lze dělat paralelně, je v~první fázi algoritmu
    spočítat paralelně počet čísel v~podintervalech, v~druhé fázi algoritmu zjišťovat paralelně, která čísla
    patří do konečného intervalu a mají se načíst, a nakonec v~poslední fázi paralelně hledat první a poslední
    pozici nalezeného percentilu.

    \subsubsection{SMP}

    V~paralelní verzi programu využívající SMP jsem použil knihovnu Intel TBB.
    Po načtení části souboru se vždy pomocí \texttt{tbb::parallel\_for} tato část rozdělí
    na kousky, který jsou poté zpracovány paralelně. Paralelní přístup k~sumám a k~sampleru
    náhodných číslel je zajištěn pomocí \texttt{tbb::combinable}. Každé vlákno má tak svoje
    vlastní instance všech potřebných struktur, které se po skončení výpočtu sloučí dohromady.

    Pro případný runtime TBB a zásobníky jednotivých vláken je rezervováno 50 MB paměti.

    \subsubsection{OpenCL}

    Při implementaci OpenCL verze programu jsem jsem paralelizoval pouze první fázi programu --
    hledání intervalu. Ostatní části by nemělo smysl na GPGPU paralelizovat, jelikož obsahují příliš málo výkonné logiky.

    Nejprve jsem na OpenCL zařízení počítal pouze kategorizaci čísel, tzn. zda je nevalidní, větší, menší nebo rovno pivotu.
    Toto lze efektivně paralelizovat, avšak penalizace za přesun dat mezi CPU a GPGPU byla
    příliš vysoká a přebila urychlení získané paralelním výpočtem.
    Přesunul jsem tedy na GPGPU celou logiku zpracování čísel, včetně volby náhodného prvku.
    Implementoval jsem kvůli tomi i jednoduchý generátor pseudonáhodných čísel přímo na GPGPU.
    Po této úpravě již bylo urychlení znatelné.

    Nejdříve se GPGPU pošle vstupní pole čísel. Tato čísla se paralelně zařadí do jedné ze 4
    kategorií: nevalidní, menší než pivot, větší než pivot, rovno pivotu. Výsledek kategorizace ze
    zapíše do mezibufferu jako číslo 0-4.

    V~dalším kroku se sečte počet čísel v~jednotlivých kategoriích a vyberou se náhodné prvky z~podintervalů.
    Tento krok se děje v~$n$ paralelních instancích ($n$ je konfigurovatelné), každá zpracuje poměrnou část dat.

    Po průchodu celým souborem se $n$ výsledných součtů a náhodně vybraných prvků oděšle zpět na CPU, kde dojde
    k~finálnímu celkovému součtu na jednom vlákně.

    Pro případný runtime OpenCL je rezervováno 210 MB paměti. Experimentálně jsem zjistil, že runtime pro
    grafickou kartu NVIDIA zabere 206 MB.

    \subsection{Optimalizace}

    Během vývoje programu jsem postupně vyzkoušel několik optimalizací pro urcyhlení výpočtu.
    Patří mezi ně následujíci:

    \begin{itemize}
        \item Inline často volaných funkcí
        \item Vytvoření vlastní funkce pro klasifikaci doublů s~použitím bitových masek
        \item Omezení/eliminace co nejvíce konkuretních zápisů/čtení ke společné paměti z~více vláken
        \item Přesun co nejvíce logiky do paralelizovaného výpočtu
        \item Použití bufferů alokovaných knihovnou OpenCL
    \end{itemize}

    Tato drobná vylepšení měla na rychlost běhu programu vliv řádově i desítky procent.


    \pagebreak

    \section{Uživatelská příručka}

    Program přebírá tři hlavní argumenty:

    \begin{itemize}
        \item cesta k~souboru
        \item pecentil -- číslo od 1--100
        \item způsob výpočtu - řetězec:
        \indent \begin{description}
                    \item["single"] - sériový výpočet
                    \item["SMP"] - paralelní výpočet na více vláknech s~použitím SMP
                    \item["název Opencl zařízení"] - paralelní výpočet na OpenCL zařízení
        \end{description}
    \end{itemize}

    Při spuštění programu bez argumentů se vypíše stručný návod na použití včetně popisu všech možných parametrů.


    \vspace{2em}

    \begin{minipage}{\textwidth}
        \begin{verbatim}
Percentile Solver version 1.0, (c) 2021 Miroslav Krysl
Usage:
     pprsolver [options] <FILE_PATH> <PERCENTILE> <SOLVER_TYPE>

     pprsolver -l

Reads file as a sequence of 64bit floating point numbers.
Finds value on the given percentile.
Finds first and last occurrence of the value in the file.
Accepts zeros and normal numbers, ignores NaNs, infinities and subnormal numbers.

Options:

<FILE_PATH>                  - Path to the file to read.
<PERCENTILE>                 - Percentile value (1 - 100).
<SOLVER_TYPE>                - Type of the solver:
                                 'single'       - single threaded
                                 'SMP'          - multi-threaded
                                 'device_name'  - OpenCL with the device of given name
-b                           - Measure and print execution time.
-B                           - Measure execution time for multiple percentiles
                               starting from 1 to 100.
                               Prints individual times and average.
                               <PERCENTILE> argument is used as percentile step.
-t <TIMEOUT>                 - Watchdog timeout in milliseconds.
                               Default: 15000 (15 s)
-M <MAX_MEMORY>              - The maximum memory used for computation in megabytes.
                               Default: 250 MB
-m <LOAD_BUFFER_SIZE>        - The size of the load buffer in megabytes.
                               Default: 1 MB
-s <STATES>                  - The number of parallel states in OpenCL computation.
                               Default: 8192
-p <THREADS>                 - The number of threads in multi-threaded computation.
                               Default: 0 (automatic number of threads)
-l                           - List available OpenCL devices.
        \end{verbatim}
    \end{minipage}
    \vspace{2em}

    \pagebreak

    \section{Měření urychlení}

    Pro otestování programu a změření urychlení jsem vytvořil čtyři náhodně vygenerované soubory o~velikosti 0,5 GB, 1 GB, 2 GB a 4 GB,
    a výpočet vždy testoval na každém z~nich.

    Vzhledem k~tomu, že je algoritmus založen na náhodných výběrech pivotů, čas výpočtu se pokaždé liší.
    Pro každou testovanou variantu nastavení výpočtu jsem tedy provedl $n$ měření (zvolil jsem 25, aby všechna měření zabrala rozumnou celkovou dobu, i tak ale měření trvala několik hodin)
    a časy zprůměroval.

    Měření jsem provedl pro všechny tři verze výpočtu, tedy pro sériovou, paralelní SMP a paralelní OpenCL.
    Každé měření jsem spustil tak, aby celková použitá paměť nezabírala více než 250 MB.

    Pro obě paralelní verze jsem spočítal velikost urychlení $S$ oproti sériové verzi pomocí následující rovnice \eqref{eq:speedup}.

    \begin{equation}\label{eq:speedup}
        S~= \frac{S_{serial}}{S_{parallel}}
    \end{equation}

    Testování probíhalo na CPU Intel i7-9750H a GPU NVIDIA GeForce GTX 1650 Mobile / Max-Q pod operačním systémem
    Fedora Linux, kernel 5.14.18.

    Velikosti urychlení pro paralelní verzi SMP (Intel CPU) jsou na grafu \ref{fig:chart1} a pro verzi OpenCL (NVIDIA GPU) na grafu \ref{fig:chart2}.

    \begin{figure}[H]
        \centering
        \includegraphics[width=\textwidth]{img/chart_smp2}
        \caption{Velikost urychlení paralelní SMP verze výpočtu}
        \label{fig:chart1}
    \end{figure}

    \begin{figure}[H]
        \centering
        \includegraphics[width=\textwidth]{img/chart_opencl2}
        \caption{Velikost urychlení paralelní OpenCL verze výpočtu}
        \label{fig:chart2}
    \end{figure}

    Z~obou grafů je vidět, že došlo k nepřílíš znatelnému urychlení i při použití více vláken. To je s~největší pravděpodobností způsobeno tím,
    že program tráví značnou část výpočtu čekání na přečtení souboru, což nelze efektivně paralelizovat.

    Pro malé soubory je urychlení prakticky minimální, výrazné je až u~vetších souborů.

    Co se týče počtu použitých vláken, u~SMP urychlení víceméně
    stagnuje již při použití 10 a více vláken. Dalo by se konstatovat, že se v~tuto chvíli začíná projevovat
    řežie potřebná pro řízení vláken.

    U~OpenCL se ukázalo jako nejefektivnější použít přibližně 2048 až 16384 vláken pro paralelní výpočet
    mezisum. Při vyšším počtu vláken se již začal projevovat čas potřebný pro finální součet sum na CPU.
    Na výsledném urychlení se také projevuje čas potřebný pro přesun dat mezi CPU a GPU.

    Dále se malé zrychlení nechá vystvětlit i tím, že obě paralelní verze potřebují určitou část paměti pro runtime.
    U OpenCL, které zabere na NVIDII cca 206 MB, má malá pamět pro buffery poté znatelný vliv na rychlost výpočtu, protože se musí provést více průchodů souborem.

    \section{Závěr}

    V~rámci této semestrální práce jsem vytvořil program pro přečtení souboru a nalezení zadaného percentilu včetně pozice prvního a posledního výskytu.

    Program počítá třemi různými způsoby: sériově, paralelně s~použitím SMP a paralelně s~použitím knihovny OpenCL.

    SMP verze dosáhla maximálního urychlení 2,5 (se souborem o velikosti 4 GB) a to při použití již 10 vláken.
    OpenCL verze dosáhla maximálního urychlení pouze 1,14 při použití 2048 paralelní stavů pro výpočet mezisum.
    Tatoto relativně malá urychlení lze vysvětlit tím, že program tráví značnou část výpočtu čekáním na čtení
    z disku a u OpenCL verze také čekáním na přenost dat mezi CPU a GPGPU. Dále je také pro obě paralelní verze
    dostupno méně paměti pro buffery, tím pádem se musí provést více průchodů souborem.

    Do programu by jistě bylo možné vnést další optimalizační prvky, avšak vzhledem k~tomu, že mnoho důležitých optimalizací bylo už provedeno,
    a také že velkou část času běhu programu tvoří operace čtení ze souboru, nemá smysl už žádně další hledat.
    Výsledné urychlení výpočtu už by nebylo tak znatelné.

\end{document}
