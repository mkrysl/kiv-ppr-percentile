#include <atomic>

#include <tbb/combinable.h>
#include <tbb/parallel_for.h>

#include "solver_parallel.h"
#include "sample.h"

namespace ppr {

    PercentileSolverParallel::PercentileSolverParallel(Watchdog watchdog,
                                                       std::size_t max_memory,
                                                       std::size_t load_buffer_size,
                                                       std::size_t threads):
            PercentileSolver(std::move(watchdog), max_memory),
            arena(threads == 0 ? tbb::task_arena::automatic : static_cast<int>(threads)) {

        if (load_buffer_size < MIN_LOAD_BUFFER_SIZE) {
            throw std::invalid_argument("load buffer is too small: " + std::to_string(load_buffer_size));
        }

        this->load_buffer_size = load_buffer_size / sizeof(double);
        this->max_interval_size = (max_memory - load_buffer_size - RUNTIME_MEMORY_RESERVE) / sizeof(double);
        this->max_buffer_size = this->load_buffer_size + this->max_interval_size;

        if (this->max_interval_size < MIN_INTERVAL_SIZE) {
            throw std::invalid_argument(
                    "final interval max size would be too small: " + std::to_string(this->max_interval_size));
        }
    }


    std::size_t PercentileSolverParallel::get_max_interval_size() {
        return max_interval_size;
    }


    PassResult PercentileSolverParallel::partition(FileReader &file, double lower, double pivot, double upper) {
        std::vector<double> buffer(max_buffer_size);

        tbb::combinable<std::size_t> lt(0);
        tbb::combinable<std::size_t> eq(0);
        tbb::combinable<std::size_t> gt(0);

        tbb::combinable<ReservoirSample<double>> random_lt(0.0);
        tbb::combinable<ReservoirSample<double>> random_gt(0.0);

        file.reset();

        // read and process all file chunks
        while (true) {
            // notify watchdog
            watchdog.notify();

            auto items_read = file.read(buffer.data(), max_buffer_size);

            if (items_read == 0) {
                // the whole file was read
                break;
            }

            auto work = [&](tbb::blocked_range<std::size_t> r) {
                std::size_t &lt_local = lt.local();
                std::size_t &eq_local = eq.local();
                std::size_t &gt_local = gt.local();

                ReservoirSample<double> &random_lt_local = random_lt.local();
                ReservoirSample<double> &random_gt_local = random_gt.local();

                for (std::size_t i = r.begin(); i < r.end(); i++) {
                    auto &d = buffer[i];

                    // filter out abnormal values
                    if (!is_normal(d)) {
                        continue;
                    }

                    // count numbers
                    if (lower < d && d < pivot) {
                        lt_local += 1;
                        random_lt_local.put(d);
                    } else if (pivot < d && d < upper) {
                        gt_local += 1;
                        random_gt_local.put(d);
                    } else if (d == pivot) {
                        eq_local += 1;
                    }
                }
            };

            // process buffer chunks in parallel
            arena.execute([&]() {
                tbb::parallel_for(tbb::blocked_range<std::size_t>(0, items_read), work);
            });
        }

        // combine back thread-local sums
        std::size_t lt_combined = lt.combine(std::plus<>());
        std::size_t eq_combined = eq.combine(std::plus<>());
        std::size_t gt_combined = gt.combine(std::plus<>());

        // combine back thread-local randoms
        ReservoirSample<double> random_lt_combined(0.0);
        random_lt.combine_each([&random_lt_combined](const ReservoirSample<double> &r) {
            random_lt_combined.combine(r);
        });

        ReservoirSample<double> random_gt_combined(0.0);
        random_gt.combine_each([&random_gt_combined](const ReservoirSample<double> &r) {
            random_gt_combined.combine(r);
        });

        return PassResult{
                lt_combined,
                eq_combined,
                gt_combined,
                random_lt_combined.get(),
                random_gt_combined.get()
        };
    }


    double PercentileSolverParallel::nth_value(
            FileReader &file,
            std::size_t index,
            double lower,
            double upper,
            std::size_t interval_size) {

        std::vector<double> load_buffer(load_buffer_size);
        std::vector<double> interval_buffer(interval_size);

        std::atomic<std::size_t> loaded = 0;

        file.reset();

        // read and load all numbers from interval
        while (true) {
            // notify watchdog
            watchdog.notify();

            std::size_t items_read = file.read(load_buffer.data(), load_buffer_size);

            if (items_read == 0) {
                // the whole file was read
                break;
            }

            auto work = [&](tbb::blocked_range<std::size_t> r) {
                for (std::size_t i = r.begin(); i < r.end(); i++) {
                    auto &d = load_buffer[i];

                    // filter out abnormal values
                    if (!is_normal(d)) {
                        continue;
                    }

                    if (lower < d && d < upper) {
                        auto pos = loaded.fetch_add(1);
                        interval_buffer[pos] = d;
                    }
                }
            };

            // process buffer chunks in parallel
            arena.execute([&]() {
                tbb::parallel_for(tbb::blocked_range<std::size_t>(0, items_read), work);
            });
        }

        std::nth_element(interval_buffer.begin(),
                         interval_buffer.begin() + static_cast<long>(index),
                         interval_buffer.end());
        return interval_buffer[index];
    }


    PositionsResult PercentileSolverParallel::search(FileReader &file, double value) {
        std::vector<double> buffer(max_buffer_size);
        tbb::combinable<Positions> positions(Positions{0, 0, false});
        std::size_t offset = 0;

        file.reset();

        // read all numbers
        while (true) {
            // notify watchdog
            watchdog.notify();

            std::size_t items_read = file.read(buffer.data(), max_buffer_size);

            if (items_read == 0) {
                // the whole file was read
                break;
            }

            auto work = [&](tbb::blocked_range<std::size_t> r) {
                Positions &positions_local = positions.local();

                for (std::size_t i = r.begin(); i < r.end(); i++) {
                    if (value == buffer[i]) {
                        std::size_t position = offset + i;

                        if (positions_local.found) {
                            if (position > positions_local.last) {
                                positions_local.last = position;
                            }
                        } else {
                            positions_local.first = position;
                            positions_local.last = positions_local.first;
                            positions_local.found = true;
                        }
                    }
                }
            };

            // process buffer chunks in parallel
            arena.execute([&]() {
                tbb::parallel_for(tbb::blocked_range<std::size_t>(0, items_read), work);
            });

            offset += items_read;
        }

        Positions positions_combined = positions.combine([](const Positions &a, const Positions &b) {
            std::size_t first = 0;
            std::size_t last = 0;
            bool found = false;

            if (a.found && b.found) {
                first = a.first <= b.first ? a.first : b.first;
                last = a.last >= b.last ? a.last : b.last;
                found = true;
            } else if (a.found) {
                first = a.first;
                last = a.last;
                found = true;
            } else if (b.found) {
                first = b.first;
                last = b.last;
                found = true;
            }

            return Positions{
                    first,
                    last,
                    found
            };
        });

        return PositionsResult{
                positions_combined.first * sizeof(double),
                positions_combined.last * sizeof(double)
        };
    }
}