#pragma once

#include <cmath>
#include <optional>

#include "io.h"
#include "watchdog.h"

namespace ppr {
    /**
     * The number of bytes in one megabyte.
     */
    const std::size_t MB = 1048576;

    /**
     * The number doubles in one megabyte.
     */
    const std::size_t MB_OF_DOUBLES = MB / sizeof(double);

    /**
     * The minimum size of load buffer in bytes.
     */
    const std::size_t MIN_LOAD_BUFFER_SIZE = 1 * MB;

    /**
     * The minimum size of the final interval.
     */
    const std::size_t MIN_INTERVAL_SIZE = MB_OF_DOUBLES;

    /**
     * A percentile number 1 - 100.
     */
    class Percentile {
        /**
         * The percentile value.
         */
        uint64_t value;

    public:
        /**
         * Default initialize a new percentile value with value = 100.
         */
        Percentile();

        /**
         * Initialize a new percentile value. Throws an std::invalid_argument exception
         * if the value is not from the interval <1, 100>.
         *
         * @param value The percentile value.
         */
        explicit Percentile(uint64_t value);

        /**
         * Get the percentile value.
         * @return The percentile value.
         */
        [[nodiscard]] uint64_t get_value() const;
    };

    /**
     * The type of percentile solver.
     */
    enum class SolverType {
        Serial,
        Parallel,
        OpenCL
    };

    /**
     * A file pass result.
     */
    struct PassResult {
        /**
         * Number of values smaller than the pivot.
         */
        const std::size_t lt;

        /**
         * Number of values equal to the pivot.
         */
        const std::size_t eq;

        /**
         * Number of values greater than the pivot.
         */
        const std::size_t gt;

        /**
         * Random value selected from values lower than the pivot.
         */
        const double random_lt;

        /**
         * Random value selected from values greater than the pivot.
         */
        const double random_gt;
    };

    /**
     * A value's positions - first and last occurrence in the file.
     */
    struct PositionsResult {
        const std::size_t first;
        const std::size_t last;
    };

    /**
     * A value's positions - first and last occurrence in the file and bool if any position was found.
     */
    struct Positions {
        std::size_t first;
        std::size_t last;
        bool found;
    };

    /**
     * A solver result - the value and its first and last occurrence in the file.
     */
    struct SolverResult {
        const double value;
        const std::size_t first;
        const std::size_t last;
    };

    /**
     * Abstract class for percentile solvers.
     *
     * A percentile solver reads the input file as a sequence of doubles and finds
     * the number on the given percentile as well as the number's first and last position
     * in the file. Any double, that is not normal or zero is ignored.
     *
     * Basic algorithm:
     * 1. Choose a random pivot number from file, lower bound = -inf, upper bound = inf
     * 2. Read file and count number of elements in each interval:
     *      - lt: lower < x < pivot
     *      - eq: x == pivot
     *      - gt: pivot < x < upper
     * 3. On first pass compute the index of the value on the percentile based on total number of elements (lt + eq + gt).
     * 4. Determine in which interval the value lies, and update pivot and bounds:
     *      - lt => upper = pivot, pivot = random number from file that lies in lt
     *      - eq => value on the percentile found = pivot --> go to 7.
     *      - gt => lower = pivot, pivot = random number from file that lies in gt
     * 5. Update the index of the value = index of the value int the selected interval.
     * 6. Does the selected interval fits into the max allowed memory?
     *      - NO -> repeat from 2.
     *      - YES -> go to 7.
     * 7. Load all values from the interval and select the nth smallest value - the value on the percentile --> go to 7.
     * 8. Find value's first and last position in the file.
     *
     * Every concrete implementation must implement logic for one file pass,
     * loading and finding the nth smallest value from interval and searching value's positions in the file.
     *
     * The percentile solver has a watchdog, which monitors the progress of the computation by checking flag from the solver.
     * Implementation of the solver must periodically notify the watchdog.
     */
    class PercentileSolver {
    public:
        /**
         * Find a value from file on the given percentile.
         *
         * @param percentile The percentile.
         * @return The result value or nullopt if the file doesn't contain any normal double.
         */
        std::optional<SolverResult> solve(FileReader &file, Percentile percentile);

        /**
         * Virtual destructor to enable subclasses to cleanup after themselves.
         */
        virtual ~PercentileSolver() noexcept;
    protected:
        /**
         * The watchdog to monitor the computation.
         */
        Watchdog watchdog;

        /**
         * The maximum memory used for computation in bytes.
         */
        std::size_t max_memory;

        /**
         * Initialize a new percentile solver.
         *
         * @param watchdog The watchdog.
         * @param max_memory The maximum memory used for computation in bytes.
         */
        explicit PercentileSolver(Watchdog watchdog, std::size_t max_memory) noexcept;

        /**
         * Get maximum size of the final interval (number of doubles).
         *
         * @return Size of the final interval.
         */
        virtual std::size_t get_max_interval_size() = 0;

        /**
         * Go through all numbers from file and find counts of numbers in intervals.
         * Also pick one random number from file for the lower interval and for the upper interval.
         *
         * @param file The file.
         * @param lower The lower bound.
         * @param lower The pivot.
         * @param upper The upper bound.
         * @return Resulting counts and random numbers.
         */
        virtual PassResult partition(FileReader &file, double lower, double pivot, double upper) = 0;

        /**
         * Load all numbers from the given interval and find nth smallest value.
         *
         * @param file The file.
         * @param index The index of the (nth) value in sorted sequence.
         * @param lower The lower bound.
         * @param upper The upper bound.
         * @param interval_size The interval size.
         * @return The found value.
         */
        virtual double
        nth_value(FileReader &file, std::size_t index, double lower, double upper, std::size_t interval_size) = 0;

        /**
         * Find first and last occurrence of the value in the file.
         *
         * @param file The file.
         * @param value The value.
         * @return Value's positions.
         */
        virtual PositionsResult search(FileReader &file, double value) = 0;
    };

    /**
     * Helper struct for bitwise converting double to long without compiler warnings.
     */
    union double_long {
        double d;
        uint64_t l;
    };

    /**
     * Check if the given double is normal or zero.
     *
     * @param d The double.
     * @return True if so, false otherwise.
     */
    inline bool is_normal(double d) {
        double_long dl{d};
        uint64_t &d_long = dl.l;

        const uint64_t exp = 0x7FF0000000000000;
        const uint64_t p_zero = 0x0000000000000000;
        const uint64_t n_zero = 0x8000000000000000;

        auto inf_or_nan = (d_long & exp) == exp;
        auto sub_or_zero = (~d_long & exp) == exp;
        auto zero = d_long == p_zero || d_long == n_zero;

        return (!inf_or_nan && !sub_or_zero) || zero;
    }

    /**
     * Get index of a percentile inside a set of n sorted elements.
     *
     * @param percentile The percentile.
     * @param n The number of elements.
     * @return The index of the percentile.
     */
    std::size_t percentile_index(Percentile percentile, size_t n);
}



