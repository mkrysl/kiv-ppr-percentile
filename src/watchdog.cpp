#include "watchdog.h"

namespace ppr {

    Watchdog::Watchdog(std::chrono::milliseconds timeout,
                       std::function<void()> divergence_callback,
                       std::function<void()> timeout_callback) noexcept:
            timeout(timeout),
            divergence_callback(std::move(divergence_callback)),
            timeout_callback(std::move(timeout_callback)),
            activity_flag(false),
            value_flag(false),
            stop_flag(false),
            condition_variable(std::make_unique<std::condition_variable>()),
            mutex(std::make_unique<std::mutex>()),
            old_value(std::numeric_limits<std::size_t>::max()),
            new_value(std::numeric_limits<std::size_t>::max()) {}


    Watchdog::Watchdog(Watchdog &&other) noexcept:
            timeout(other.timeout),
            divergence_callback(other.divergence_callback),
            timeout_callback(other.timeout_callback),
            activity_flag(other.activity_flag),
            value_flag(other.value_flag),
            stop_flag(other.stop_flag),
            condition_variable(std::move(other.condition_variable)),
            mutex(std::move(other.mutex)),
            thread(std::move(other.thread)),
            old_value(other.old_value),
            new_value(other.new_value) {}


    void Watchdog::run() {
        stop_flag = false;
        activity_flag = false;
        value_flag = false;

        // run watchdog in separate thread
        thread = std::thread([this]() {
            std::unique_lock<std::mutex> lock(*mutex);

            while (!stop_flag) {
                bool result = condition_variable->wait_for(lock, timeout, [this]() {
                    return activity_flag || value_flag || stop_flag;
                });

                if (!result) {
                    // watched subject timed out
                    timeout_callback();
                }

                if (activity_flag) {
                    activity_flag = false;
                }

                if (value_flag) {
                    if (new_value >= old_value) {
                        // watched subject diverges
                        divergence_callback();
                    }

                    old_value = new_value;
                    value_flag = false;
                }
            }
        });
    }


    void Watchdog::stop() {
        std::unique_lock<std::mutex> lock(*mutex);
        stop_flag = true;
        condition_variable->notify_one();
        lock.unlock();

        if (thread.joinable()) {
            thread.join();
        }
    }


    void Watchdog::notify() {
        std::lock_guard<std::mutex> lock(*mutex);
        activity_flag = true;
        condition_variable->notify_one();
    }


    void Watchdog::notify(std::size_t value) {
        std::lock_guard<std::mutex> lock(*mutex);
        value_flag = true;
        new_value = value;
        condition_variable->notify_one();
    }
}
