#include "solver_serial.h"
#include "sample.h"

#include <string>

namespace ppr {

    PercentileSolverSerial::PercentileSolverSerial(Watchdog watchdog,
                                                   std::size_t max_memory,
                                                   std::size_t load_buffer_size) :
            PercentileSolver(std::move(watchdog), max_memory) {

        if (load_buffer_size < MIN_LOAD_BUFFER_SIZE) {
            throw std::invalid_argument("load buffer is too small: " + std::to_string(load_buffer_size));
        }

        this->load_buffer_size = load_buffer_size / sizeof(double);
        this->max_interval_size = (max_memory - load_buffer_size - RUNTIME_MEMORY_RESERVE) / sizeof(double);
        this->max_buffer_size = this->load_buffer_size + this->max_interval_size;

        if (this->max_interval_size < MIN_INTERVAL_SIZE) {
            throw std::invalid_argument(
                    "final interval max size would be too small: " + std::to_string(this->max_interval_size));
        }
    }


    std::size_t PercentileSolverSerial::get_max_interval_size() {
        return max_interval_size;
    }


    PassResult PercentileSolverSerial::partition(FileReader &file, double lower, double pivot, double upper) {
        std::vector<double> buffer(max_buffer_size);

        std::size_t lt = 0;
        std::size_t eq = 0;
        std::size_t gt = 0;

        ReservoirSample<double> random_lt(0.0);
        ReservoirSample<double> random_gt(0.0);

        file.reset();

        // read and process all file chunks
        while (true) {
            // notify watchdog
            watchdog.notify();

            auto items_read = file.read(buffer.data(), buffer.size());

            if (items_read == 0) {
                // the whole file was read
                break;
            }

            for (std::size_t i = 0; i < items_read; i++) {
                auto &d = buffer[i];

                // filter out abnormal values
                if (!is_normal(d)) {
                    continue;
                }

                // count numbers
                if (lower < d && d < pivot) {
                    lt += 1;
                    random_lt.put(d);
                } else if (pivot < d && d < upper) {
                    gt += 1;
                    random_gt.put(d);
                } else if (d == pivot) {
                    eq += 1;
                }
            }
        }

        return PassResult{
                lt,
                eq,
                gt,
                random_lt.get(),
                random_gt.get()
        };
    }


    double PercentileSolverSerial::nth_value(
            FileReader &file,
            std::size_t index,
            double lower,
            double upper,
            std::size_t interval_size) {

        std::vector<double> load_buffer(load_buffer_size);
        std::vector<double> interval_buffer(interval_size);

        std::size_t loaded = 0;

        file.reset();

        // read and load all numbers from interval
        while (true) {
            // notify watchdog
            watchdog.notify();

            std::size_t items_read = file.read(load_buffer.data(), load_buffer_size);

            if (items_read == 0) {
                // the whole file was read
                break;
            }

            for (std::size_t i = 0; i < items_read; i++) {
                auto &d = load_buffer[i];

                // filter out abnormal values
                if (!is_normal(d)) {
                    continue;
                }

                if (lower < d && d < upper) {
                    interval_buffer[loaded] = d;
                    loaded += 1;
                }
            }
        }

        std::nth_element(interval_buffer.begin(),
                         interval_buffer.begin() + static_cast<long>(index),
                         interval_buffer.end());
        return interval_buffer[index];
    }


    PositionsResult PercentileSolverSerial::search(FileReader &file, double value) {
        std::vector<double> buffer(max_buffer_size);

        bool first_found = false;
        std::size_t first = 0;
        std::size_t last = 0;

        std::size_t offset = 0;

        file.reset();

        // read all numbers
        while (true) {
            // notify watchdog
            watchdog.notify();

            std::size_t items_read = file.read(buffer.data(), max_buffer_size);

            if (items_read == 0) {
                // the whole file was read
                break;
            }

            for (std::size_t i = 0; i < items_read; i++) {
                if (value == buffer[i]) {
                    if (first_found) {
                        last = offset + i;
                    } else {
                        first = offset + i;
                        first_found = true;
                        last = first;
                    }
                }
            }

            offset += items_read;
        }

        return PositionsResult{
                first * sizeof(double),
                last * sizeof(double)
        };
    }
}