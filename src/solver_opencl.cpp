#include "solver_opencl.h"
#include "sample.h"

#include <CL/cl.h>
#include <iostream>

#include "CL/cl.hpp"

namespace ppr {

    std::string program_src = R"CLC(

    // Structure for selecting random double from a sequence of unknown size.
    struct __attribute__ ((packed)) Sample {
        uint seed;
        ulong count;
        ulong next;
        double weight;
        double current;
    };

    // Struct for storing state of the computation.
    struct __attribute__ ((packed)) State {
        ulong lt;
        ulong eq;
        ulong gt;
        struct Sample random_lt;
        struct Sample random_gt;
    };

    // Struct for storing result from one state.
    struct __attribute__ ((packed)) Result {
        ulong lt;
        ulong eq;
        ulong gt;
        double random_lt;
        double random_gt;
    };

    // Generate next pseudorandom number from seed.
    uint rand_uint(uint seed){
        seed = (seed ^ 61) ^ (seed >> 16);
        seed *= 9;
        seed = seed ^ (seed >> 4);
        seed *= 0x27d4eb2d;
        seed = seed ^ (seed >> 15);
        return seed;
    }

    // Generate pseudorandom double between 0.0 and 1.0 from seed.
    double rand_double(uint seed){
        uint max = -1;
        return ((double)seed) / ((double)max);
    }

    // Initialize the sample with seed.
    void sample_init(struct Sample *s, uint seed) {
        s->seed = seed;
        s->count = 0;
        s->next = 0;
        s->weight = 1.0;
    }

    // Insert a next value from sequence.
    void sample_put(struct Sample *s, double value) {
        if (s->next == s->count) {
            s->current = value;

            double random;

            // update weight
            s->seed = rand_uint(s->seed);
            random = rand_double(s->seed);
            s->weight *= exp(log(random));

            // update next index
            s->seed = rand_uint(s->seed);
            random = rand_double(s->seed);
            s->next += floor(log(random) / log(1.0 - s->weight)) + 1;
        }

        s->count += 1;
    }

    // Initialize a state with seeds.
    void init_state(struct State *s, uint seed_lt, uint seed_gt) {
        s->lt = 0;
        s->eq = 0;
        s->gt = 0;

        sample_init(&s->random_lt, seed_lt);
        sample_init(&s->random_gt, seed_gt);
    }

    // Initialize an array of states.
    __kernel void init(
            __global struct State *states,
            const uint seed) {
        int i = get_global_id(0);

        const uint seed_lt = rand_uint(seed + i + 1);
        const uint seed_gt = rand_uint(seed_lt + i + 1);

        struct State state = states[i];
        init_state(&state, seed_lt, seed_gt);

        states[i] = state;
    }

    // Categorize doubles from the input array.
    __kernel void categorize(
                __global const double *input,
                __global uchar *categories,
                const double lower,
                const double pivot,
                const double upper) {
        const int i = get_global_id(0);

        double value_double = input[i];
        ulong value_long = as_ulong(value_double);

        const ulong exp = 0x7FF0000000000000;
        const ulong p_zero = 0x0000000000000000;
        const ulong n_zero = 0x8000000000000000;

        bool inf_or_nan = (value_long & exp) == exp;
        bool sub_or_zero = (~value_long & exp) == exp;
        bool zero = value_long == p_zero || value_long == n_zero;

        bool normal = (!inf_or_nan && !sub_or_zero) || zero;

        bool lt = lower < value_double && value_double < pivot;
        bool gt = pivot < value_double && value_double < upper;
        bool eq = value_double == pivot;

        categories[i] = normal * (lt + gt * 2 + eq * 3);
    }

    // Aggregate categorized doubles into work states.
    __kernel void aggregate(
            __global const double *input,
            __global const uchar *categories,
            __global struct State *states,
            const ulong n) {

        const int i = get_global_id(0);
        const int s = get_global_size(0);

        struct State state = states[i];

        const int k = n / s + (n % s != 0);

        const ulong min = k * i;
        const ulong max = min + k;

        for (int j = min; j < max && j < n; j++) {
            const uchar category = categories[j];

            if (category == 1) {
                state.lt += 1;
                sample_put(&state.random_lt, input[j]);
            }
            else if (category == 2) {
                state.gt += 1;
                sample_put(&state.random_gt, input[j]);
            }
            else if (category == 3) {
                state.eq += 1;
            }
        }

        states[i] = state;
    }

    // Collect the results.
    __kernel void collect(
            __global const struct State *states,
            __global struct Result *results) {
        const int i = get_global_id(0);

        struct State state = states[i];

        struct Result result;
        result.lt = state.lt;
        result.eq = state.eq;
        result.gt = state.gt;
        result.random_lt = state.random_lt.current;
        result.random_gt = state.random_gt.current;

        results[i] = result;
    }

    )CLC";


    std::string kernel_init_name = "init";
    std::string kernel_categorize_name = "categorize";
    std::string kernel_aggregate_name = "aggregate";
    std::string kernel_collect_name = "collect";


    PercentileSolverOpenCL::PercentileSolverOpenCL(Watchdog watchdog,
                                                   std::size_t max_memory,
                                                   std::size_t load_buffer_size,
                                                   std::size_t states_count,
                                                   cl::Device device) :
            PercentileSolver(std::move(watchdog), max_memory),
            states_count(states_count),
            device(std::move(device)) {

        if (states_count == 0) {
            throw std::invalid_argument("states count must not be 0");
        }

        if (load_buffer_size < MIN_LOAD_BUFFER_SIZE) {
            throw std::invalid_argument("load buffer is too small: " + std::to_string(load_buffer_size));
        }

        this->load_buffer_size = load_buffer_size / sizeof(double);
        this->max_interval_size = (max_memory - load_buffer_size - RUNTIME_MEMORY_RESERVE) / sizeof(double);
        this->max_buffer_size = this->load_buffer_size + this->max_interval_size;

        if (this->max_interval_size < MIN_INTERVAL_SIZE) {
            throw std::invalid_argument(
                    "final interval max size would be too small: " + std::to_string(this->max_interval_size));
        }


        // check if there is enough memory for states buffer
        if (states_count * sizeof(ProgramResult) > this->max_buffer_size) {
            throw std::invalid_argument("not enough memory for OpenCL buffers");
        }

        // io buffer size = number of doubles
        this->io_buffer_size = (this->max_buffer_size * sizeof(double) - states_count * sizeof(ProgramResult))
                               / (sizeof(double) + sizeof(uint8_t));

        if (this->io_buffer_size < MIN_LOAD_BUFFER_SIZE) {
            throw std::invalid_argument("not enough memory for OpenCL IO buffers");
        }

        // check if the io buffer is sufficient for results
        if (io_buffer_size * sizeof(double) < states_count * sizeof(ProgramResult)) {
            throw std::invalid_argument("not enough memory for OpenCL states buffer");
        }

        // setup random
        std::random_device r;
        random_engine.seed(r());

        cl_int error;

        // setup OpenCL context
        context = cl::Context(this->device, nullptr, nullptr, nullptr, &error);
        if (error != CL_SUCCESS) {
            throw std::runtime_error("error while creating the OpenCL context: " + std::to_string(error));
        }

        // build OpenCL program
        program = cl::Program(context, program_src);
        error = program.build(std::vector{this->device});

        if (error != CL_BUILD_SUCCESS) {
            std::cerr << "OpenCL program build error: " << error << std::endl
                      << "log:" << std::endl
                      << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(this->device) << std::endl;

            throw std::runtime_error("error while building the OpenCL program: " + std::to_string(error));
        }

        // setup kernels
        kernel_init = cl::Kernel(program, kernel_init_name.c_str(), nullptr);
        kernel_categorize = cl::Kernel(program, kernel_categorize_name.c_str(), nullptr);
        kernel_aggregate = cl::Kernel(program, kernel_aggregate_name.c_str(), nullptr);
        kernel_collect = cl::Kernel(program, kernel_collect_name.c_str(), nullptr);
    }


    std::size_t PercentileSolverOpenCL::get_max_interval_size() {
        return max_interval_size;
    }


    PassResult
    PercentileSolverOpenCL::partition(FileReader &file, double lower, double pivot, double upper) {

        // ------- OpenCL setup -------

        cl_int error;

        // setup command queue
        cl::CommandQueue queue = cl::CommandQueue(context, this->device, 0, &error);
        if (error != CL_SUCCESS) {
            throw std::runtime_error("error while creating the OpenCL command queue: " + std::to_string(error));
        }

        // setup io buffer
        cl::Buffer io_buffer = cl::Buffer(context,
                                          CL_MEM_READ_WRITE,
                                          io_buffer_size * sizeof(double),
                                          nullptr,
                                          &error);
        if (error != CL_SUCCESS) {
            throw std::runtime_error("error while initializing the OpenCL io buffer: " + std::to_string(error));
        }

        // setup categories buffer
        cl::Buffer categories_buffer = cl::Buffer(
                context,
                CL_MEM_READ_WRITE,
                io_buffer_size * sizeof(uint8_t),
                nullptr,
                &error);
        if (error != CL_SUCCESS) {
            throw std::runtime_error(
                    "error while initializing the OpenCL categories buffer: " + std::to_string(error));
        }

        // setup states buffer
        cl::Buffer states_buffer = cl::Buffer(
                context,
                CL_MEM_READ_WRITE,
                states_count * sizeof(ProgramState),
                nullptr,
                &error);
        if (error != CL_SUCCESS) {
            throw std::runtime_error(
                    "error while initializing the OpenCL states buffer: " + std::to_string(error));
        }

        // setup kernels arguments
        kernel_init.setArg(0, states_buffer);

        kernel_categorize.setArg(0, io_buffer);
        kernel_categorize.setArg(1, categories_buffer);

        kernel_aggregate.setArg(0, io_buffer);
        kernel_aggregate.setArg(1, categories_buffer);
        kernel_aggregate.setArg(2, states_buffer);

        kernel_collect.setArg(0, states_buffer);
        kernel_collect.setArg(1, io_buffer);


        // ------- Partition algorithm -------

        // init GPGPU state structures
        uint32_t seed = random_engine();
        kernel_init.setArg(1, seed);
        queue.enqueueNDRangeKernel(kernel_init, cl::NullRange, cl::NDRange(states_count));
        queue.finish();

        file.reset();

        // read and process all file chunks
        while (true) {
            // notify watchdog
            watchdog.notify();

            // map io buffer for writing
            auto *io_buffer_ptr = reinterpret_cast<uint8_t *>(
                    queue.enqueueMapBuffer(io_buffer, CL_TRUE, CL_MAP_WRITE, 0, io_buffer_size * sizeof(double), nullptr,
                                           nullptr, &error));
            if (error != CL_SUCCESS) {
                throw std::runtime_error("error while mapping the OpenCL io host buffer: " + std::to_string(error));
            }

            // read file chunk
            auto items_read = file.read(reinterpret_cast<double *>(io_buffer_ptr), io_buffer_size);

            // unmap io buffer
            queue.enqueueUnmapMemObject(io_buffer, io_buffer_ptr);

            if (items_read == 0) {
                // the whole file was read
                break;
            }

            // enqueue kernel for categorization
            kernel_categorize.setArg(2, lower);
            kernel_categorize.setArg(3, pivot);
            kernel_categorize.setArg(4, upper);
            queue.enqueueNDRangeKernel(kernel_categorize, cl::NullRange, cl::NDRange(items_read));

            // enqueue kernel for aggregation
            kernel_aggregate.setArg(3, items_read);
            queue.enqueueNDRangeKernel(kernel_aggregate, cl::NullRange, cl::NDRange(states_count));
        }

        // enqueue kernel for collecting results
        queue.enqueueNDRangeKernel(kernel_collect, cl::NullRange, cl::NDRange(states_count));

        // map io buffer for reading
        auto *io_buffer_ptr = reinterpret_cast<uint8_t *>(
                queue.enqueueMapBuffer(io_buffer, CL_TRUE, CL_MAP_READ, 0, io_buffer_size * sizeof(double), nullptr,
                                       nullptr, &error));
        if (error != CL_SUCCESS) {
            throw std::runtime_error("error while mapping the OpenCL io host buffer: " + std::to_string(error));
        }

        std::size_t lt = 0;
        std::size_t eq = 0;
        std::size_t gt = 0;

        ReservoirSample<double> random_lt(0.0);
        ReservoirSample<double> random_gt(0.0);

        auto *buffer = reinterpret_cast<ProgramResult *>(io_buffer_ptr);

        // combine results from all states
        for (std::size_t i = 0; i < states_count; ++i) {
            ProgramResult &result = buffer[i];

            lt += result.lt;
            eq += result.eq;
            gt += result.gt;

            random_lt.combine(result.lt, result.random_lt);
            random_gt.combine(result.gt, result.random_gt);
        }

        // unmap io buffer
        queue.enqueueUnmapMemObject(io_buffer, io_buffer_ptr);

        return PassResult{
                lt,
                eq,
                gt,
                random_lt.get(),
                random_gt.get()
        };
    }


    double PercentileSolverOpenCL::nth_value(
            FileReader &file,
            std::size_t index,
            double lower,
            double upper,
            std::size_t interval_size) {

        std::vector<double> load_buffer(load_buffer_size);
        std::vector<double> interval_buffer(interval_size);
        std::size_t loaded = 0;

        file.reset();

        // read and load all numbers from interval_buffer
        while (true) {
            // notify watchdog
            watchdog.notify();

            std::size_t items_read = file.read(load_buffer.data(), load_buffer_size);

            if (items_read == 0) {
                // the whole file was read
                break;
            }

            for (std::size_t i = 0; i < items_read; i++) {
                auto &d = load_buffer[i];

                // filter out abnormal values
                if (!is_normal(d)) {
                    continue;
                }

                if (lower < d && d < upper) {
                    interval_buffer[loaded] = d;
                    loaded += 1;
                }
            }
        }

        std::nth_element(interval_buffer.begin(),
                         interval_buffer.begin() + static_cast<long>(index),
                         interval_buffer.end());
        return interval_buffer[index];
    }


    PositionsResult PercentileSolverOpenCL::search(FileReader &file, double value) {
        std::vector<double> buffer(max_buffer_size);

        bool first_found = false;
        std::size_t first = 0;
        std::size_t last = 0;

        std::size_t offset = 0;

        file.reset();

        // read all numbers
        while (true) {
            // notify watchdog
            watchdog.notify();

            std::size_t items_read = file.read(buffer.data(), max_buffer_size);

            if (items_read == 0) {
                // the whole file was read
                break;
            }

            for (std::size_t i = 0; i < items_read; i++) {
                if (value == buffer[i]) {
                    if (first_found) {
                        last = offset + i;
                    } else {
                        first = offset + i;
                        first_found = true;
                        last = first;
                    }
                }
            }

            offset += items_read;
        }

        return PositionsResult{
                first * sizeof(double),
                last * sizeof(double)
        };
    }


    void list_devices() {
        std::vector<cl::Platform> platforms;
        cl::Platform::get(&platforms);

        for (unsigned int p = 0; p < platforms.size(); p++) {
            cl::Platform &platform = platforms[p];

            std::wcout << "Platform [" << p << "]: " << platform.getInfo<CL_PLATFORM_NAME>().c_str() << std::endl;

            std::vector<cl::Device> devices;
            platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);

            for (unsigned int d = 0; d < devices.size(); d++) {
                cl::Device &device = devices[d];
                std::wcout << "\tDevice [" << d << "]: " << device.getInfo<CL_DEVICE_NAME>().c_str() << std::endl;
            }
        }
    }


    std::optional<cl::Device> get_device(const std::string &name) {
        std::vector<cl::Platform> platforms;
        cl::Platform::get(&platforms);

        for (auto &platform: platforms) {
            std::vector<cl::Device> devices;
            platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);

            for (auto &device: devices) {
                std::string device_name = device.getInfo<CL_DEVICE_NAME>().c_str();

                if (device_name == name) {
                    return device;
                }
            }
        }

        return std::nullopt;
    }
}
