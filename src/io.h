#pragma once

#include <filesystem>
#include <fstream>

namespace ppr {

    /**
     * Class for reading file chunks.
     */
    class FileReader {
    private:

        /**
         * The file input stream.
         */
        std::ifstream file;
    public:
        /**
         * File reader default constructor.
         */
        FileReader() = default;

        /**
         * Create a new file reader that reads from file on the given path.
         *
         * @param file_path
         */
        explicit FileReader(const std::filesystem::path &file_path);

        /**
         * Set reading position to the beginning of the file.
         */
        void reset();

        /**
         * Read `size` bytes into the buffer from the file current position and move the position after after the read bytes.
         */
        template<typename T>
        std::size_t read(T *buffer, std::size_t size);
    };


    template<typename T>
    std::size_t FileReader::read(T *buffer, std::size_t size) {
        file.read(reinterpret_cast<char *>(buffer), size * sizeof(T));

        if (file.bad()) {
            throw std::runtime_error("Error while reading the file.");
        }

        return file.gcount() / sizeof(T);
    }
}
