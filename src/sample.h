#pragma once

#include <random>

namespace ppr {

    /**
     * Class for random sampling. It selects random item from a sequence
     * of arbitrary length. The probability distribution of selecting nth item
     * is uniform.
     *
     * @tparam T The type of sequence items.
     */
    template<typename T>
    class ReservoirSample {
    private:
        /**
         * Real distribution between 0.0 and 1.0.
         */
        static std::uniform_real_distribution<double> real_distribution;

        /**
         * Instance of a random engine.
         */
        std::default_random_engine random_engine;

        /**
         * The total number of inserted items.
         */
        std::size_t count;

        /**
         * The index of item which will be selected next.
         */
        std::size_t next;

        /**
         * Intermediate weight value for computing index of the next item.
         */
        double weight;

        /**
         * The currently selected value (or initial item if no items were inserted)
         */
        T current;
    public:
        /**
         * Initialize a new random sampler.
         *
         * @param init_value The initial value.
         */
        explicit ReservoirSample(T init_value) noexcept;

        /**
         * Insert next item.
         * @param value
         */
        void put(T value);

        /**
         * Combine this sample with other sample.
         *
         * @param other The other sample.
         */
        void combine(const ReservoirSample<T> &other);

        /**
         * Combine this sample with other sample of sequence `size` and selected `value`.
         *
         * @param size Size of the sequence to combine.
         * @param value Selected value from the sequence to combine.
         */
        void combine(std::size_t size, T value);

        /**
         * Get the currently selected item.
         *
         * @return Selected item or the initial item if no items were inserted.
         */
        T get() const;

    private:
        /**
         * Update index to a next selected value `next` to a random value greater than or equal to `count`.
         */
        void update_next();
    };


    template<typename T>
    std::uniform_real_distribution<double>
            ReservoirSample<T>::real_distribution = std::uniform_real_distribution<double>(0, 1);


    template<typename T>
    ReservoirSample<T>::ReservoirSample(T init_value) noexcept:
            count(0),
            next(0),
            weight(1.0),
            current(init_value) {
        std::random_device r;
        random_engine.seed(r());
    }


    template<typename T>
    inline void ReservoirSample<T>::update_next() {
        while(next < count) {
            double random;

            // update weight
            random = real_distribution(random_engine);
            weight *= std::exp(std::log(random));

            // update next index
            random = real_distribution(random_engine);
            next += static_cast<std::size_t>(std::floor(std::log(random) / std::log(1.0 - weight)) + 1);
        }
    }


    template<typename T>
    inline void ReservoirSample<T>::put(T value) {
        if (next == count) {
            current = value;
            count += 1;

            update_next();
        } else {
            count += 1;
        }
    }


    template<typename T>
    void ReservoirSample<T>::combine(const ReservoirSample<T> &other) {
        combine(other.count, other.current);
    }


    template<typename T>
    void ReservoirSample<T>::combine(std::size_t size, T value) {
        if (count == 0) {
            current = value;
        } else if (size != 0) {
            double p = (double) size / (double) (count + size);
            std::bernoulli_distribution b(p);

            bool select_this = b(random_engine);

            if (select_this) {
                current = value;
            }
        }

        count += size;
        update_next();
    }

    template<typename T>
    T ReservoirSample<T>::get() const {
        return current;
    }
}