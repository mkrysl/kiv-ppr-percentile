#pragma once

#include <optional>

#include <random>

#include "CL/cl.hpp"
#include "solver.h"

namespace ppr {

    // region OpenCL program structures

#pragma pack(push, 1)

    /**
     * Mapping OpenCL structure.
     */
    struct ProgramSample {
        uint32_t seed;
        uint64_t count;
        uint64_t next;
        double weight;
        double current;
    };

    /**
     * Mapping OpenCL structure.
     */
    struct ProgramResult {
        uint64_t lt;
        uint64_t eq;
        uint64_t gt;
        double random_lt;
        double random_gt;
    };

    /**
     * Mapping OpenCL structure.
     */
    struct ProgramState {
        uint64_t lt;
        uint64_t eq;
        uint64_t gt;
        ProgramSample random_lt;
        ProgramSample random_gt;
    };

#pragma pack(pop)

    // endregion


    /**
     * OpenCL percentile solver.
     */
    class PercentileSolverOpenCL : public PercentileSolver {
    private:
        /**
         * The random engine.
         */
        std::default_random_engine random_engine;

        /**
         * The maximum size of the final interval.
         */
        std::size_t max_interval_size;

        /**
         * The size of load buffer for the final interval loading.
         */
        std::size_t load_buffer_size;

        /**
         * The maximum total size of all buffers.
         */
        std::size_t max_buffer_size;

        /**
         * The size of the io buffer.
         */
        size_t io_buffer_size;

        /**
         * The number of states that collects sums in parallel.
         */
        size_t states_count;

        /**
         * The OpenCL device.
         */
        cl::Device device;

        /**
         * The OpenCL context.
         */
        cl::Context context;

        /**
         * The OpenCL solver program.
         */
        cl::Program program;

        /**
         * The OpenCL initialization kernel.
         */
        cl::Kernel kernel_init;

        /**
         * The OpenCL categorization kernel.
         */
        cl::Kernel kernel_categorize;

        /**
         * The OpenCL aggregation kernel.
         */
        cl::Kernel kernel_aggregate;

        /**
         * The OpenCL collecting kernel.
         */
        cl::Kernel kernel_collect;

    public:
        /**
         * The size of memory in bytes reserved for runtime.
         */
        static const std::size_t RUNTIME_MEMORY_RESERVE = 210 * MB;

        /**
         * Initialize an OpenCL percentile solver.
         *
         * Total size of the GPGPU io buffer is `max_interval_size` + `load_buffer_size`.
         *
         * @param watchdog The watchdog.
         * @param max_memory The maximum memory used for computation in bytes.
         * @param load_buffer_size The size of the load buffer for final interval loading in bytes.
         * @param states_count The number of parallel states on the GPGPU for aggregating results.
         * @param device The OpenCL device to run on.
         */
        explicit PercentileSolverOpenCL(Watchdog watchdog,
                                        std::size_t max_memory,
                                        std::size_t load_buffer_size,
                                        std::size_t states_count,
                                        cl::Device device);

        std::size_t get_max_interval_size() override;

        PassResult partition(FileReader &file, double lower, double pivot, double upper) override;

        double
        nth_value(FileReader &file, std::size_t index, double lower, double upper, std::size_t interval_size) override;

        PositionsResult search(FileReader &file, double value) override;
    };

    /**
     * List all OpenCL platforms and devices.
     */
    void list_devices();

    /**
     * Get an OpenCL device by name.
     * @param name The device name.
     * @return The device or nullopt if not present.
     */
    std::optional<cl::Device> get_device(const std::string &name);
}