#include <iostream>
#include <chrono>
#include <string>
#include <cstring>

#include "io.h"
#include "watchdog.h"
#include "solver.h"
#include "solver_serial.h"
#include "solver_parallel.h"
#include "solver_opencl.h"

/**
 * Percentile solver program config.
 */
struct Config {
    std::string file_path;
    ppr::Percentile percentile;
    ppr::SolverType solver_type{ppr::SolverType::Serial};
    std::string device_name;
    std::size_t timeout_ms{15000};
    std::size_t max_memory{ppr::MB * 250};
    std::size_t load_buffer_size{ppr::MB * 1};
    std::size_t states{8192};
    std::size_t threads{0};
    bool measure_one{false};
    bool measure_all{false};
};


/**
 * Print program usage.
 */
void print_usage() {
    std::wcout << "Percentile Solver version 1.0, (c) 2021 Miroslav Krysl" << std::endl;
    std::wcout << "Usage:" << std::endl;
    std::wcout << "     pprsolver [options] <FILE_PATH> <PERCENTILE> <SOLVER_TYPE>" << std::endl;
    std::wcout << std::endl;
    std::wcout << "     pprsolver -l" << std::endl;
    std::wcout << std::endl;
    std::wcout << "Reads file as a sequence of 64bit floating point numbers." << std::endl;
    std::wcout << "Finds value on the given percentile." << std::endl;
    std::wcout << "Finds first and last occurrence of the value in the file." << std::endl;
    std::wcout << "Accepts zeros and normal numbers, ignores NaNs, infinities and subnormal numbers." << std::endl;
    std::wcout << std::endl;
    std::wcout << "Options:" << std::endl;
    std::wcout << std::endl;
    std::wcout << "<FILE_PATH>                  - Path to the file to read." << std::endl;
    std::wcout << "<PERCENTILE>                 - Percentile value (1 - 100)." << std::endl;
    std::wcout << "<SOLVER_TYPE>                - Type of the solver:" << std::endl;
    std::wcout << "                                 'single'       - single threaded" << std::endl;
    std::wcout << "                                 'SMP'          - multi-threaded" << std::endl;
    std::wcout << "                                 'device_name'  - OpenCL with the device of given name" << std::endl;
    std::wcout << "-b                           - Measure and print execution time." << std::endl;
    std::wcout << "-B                           - Measure execution time for multiple percentiles" << std::endl;
    std::wcout << "                               starting from 1 to 100." << std::endl;
    std::wcout << "                               Prints individual times and average." << std::endl;
    std::wcout << "                               <PERCENTILE> argument is used as percentile step." << std::endl;
    std::wcout << "-t <TIMEOUT>                 - Watchdog timeout in milliseconds." << std::endl;
    std::wcout << "                               Default: 15000 (15 s)" << std::endl;
    std::wcout << "-M <MAX_MEMORY>              - The maximum memory used for computation in megabytes." << std::endl;
    std::wcout << "                               Default: 250 MB" << std::endl;
    std::wcout << "-m <LOAD_BUFFER_SIZE>        - The size of the load buffer in megabytes." << std::endl;
    std::wcout << "                               Default: 1 MB" << std::endl;
    std::wcout << "-s <STATES>                  - The number of parallel states in OpenCL computation." << std::endl;
    std::wcout << "                               Default: 8192" << std::endl;
    std::wcout << "-p <THREADS>                 - The number of threads in multi-threaded computation." << std::endl;
    std::wcout << "                               Default: 0 (automatic number of threads)" << std::endl;
    std::wcout << "-l                           - List available OpenCL devices." << std::endl;
}


/**
 * Parse program config from arguments.
 * @param argc
 * @param argv
 * @return Parsed config.
 */
Config parse_config(int argc, char **argv) {

    Config config;
    int i = 1;

    if (argc == 1) {
        print_usage();
        std::exit(0);
    }

    // parse options
    for (; i < argc; ++i) {
        if (argv[i][0] != '-') {
            break;
        }

        if (std::strlen(argv[i]) == 1) {
            std::wcout << "invalid option \"-\"" << std::endl;
            std::exit(1);
        }

        switch (argv[i][1]) {
            case 'l':
                if (i != 1) {
                    std::wcout << "option \"-l\" can only be used alone" << std::endl;
                    std::exit(1);
                }

                ppr::list_devices();
                std::exit(0);
            case 'b':
                config.measure_one = true;
                break;
            case 'B':
                config.measure_all = true;
                break;
            case 't':
                i++;

                if (i == argc) {
                    std::wcout << "missing timeout value for option \"-t\"" << std::endl;
                }

                try {
                    std::size_t timeout = std::stoul(argv[i]);
                    config.timeout_ms = timeout;
                }
                catch (const std::exception &e) {
                    std::wcout << "invalid timeout value for option \"-t\"" << std::endl;
                    std::exit(1);
                }
                break;
            case 'm':
                i++;

                if (i == argc) {
                    std::wcout << "missing load buffer size value for option \"-m\"" << std::endl;
                }

                try {
                    std::size_t load_buffer_size = std::stoul(argv[i]);
                    config.load_buffer_size = load_buffer_size * ppr::MB;
                }
                catch (const std::exception &e) {
                    std::wcout << "invalid load buffer size value for option \"-m\"" << std::endl;
                    std::exit(1);
                }
                break;
            case 'M':
                i++;

                if (i == argc) {
                    std::wcout << "missing memory size value for option \"-M\"" << std::endl;
                }

                try {
                    std::size_t max_memory = std::stoul(argv[i]);
                    config.max_memory = max_memory * ppr::MB;
                }
                catch (const std::exception &e) {
                    std::wcout << "invalid memory size value for option \"-M\"" << std::endl;
                    std::exit(1);
                }
                break;
            case 's':
                i++;

                if (i == argc) {
                    std::wcout << "missing states number value for option \"-s\"" << std::endl;
                }

                try {
                    std::size_t states = std::stoul(argv[i]);
                    config.states = states;
                }
                catch (const std::exception &e) {
                    std::wcout << "invalid states number value for option \"-s\"" << std::endl;
                    std::exit(1);
                }
                break;
            case 'p':
                i++;

                if (i == argc) {
                    std::wcout << "missing threads number value for option \"-p\"" << std::endl;
                }

                try {
                    std::size_t threads = std::stoul(argv[i]);
                    config.threads = threads;
                }
                catch (const std::exception &e) {
                    std::wcout << "invalid threads number value for option \"-p\"" << std::endl;
                    std::exit(1);
                }
                break;
        }
    }

    if (argc - i < 3) {
        std::wcout << "too few arguments" << std::endl;
    }

    // parse file path
    config.file_path = argv[i];
    i++;

    // parse percentile number
    try {
        uint64_t percentile = std::stoul(argv[i]);
        config.percentile = ppr::Percentile(percentile);
    }
    catch (const std::invalid_argument &e) {
        std::wcout << "invalid percentile value: " << e.what() << std::endl;
        std::exit(1);
    }
    i++;

    // parse solver type
    if (std::strncmp("single", argv[i], 5) == 0) {
        config.solver_type = ppr::SolverType::Serial;
    } else if (std::strncmp("SMP", argv[i], 3) == 0) {
        config.solver_type = ppr::SolverType::Parallel;
    } else {
        config.solver_type = ppr::SolverType::OpenCL;
        config.device_name = argv[i];
    }

    return config;
}

/**
 * Run solver.
 * @param config The solver configuration.
 */
void solve(const Config &config) {
    // setup watchdog
    ppr::Watchdog watchdog(std::chrono::milliseconds(config.timeout_ms),
                           []() {
                               std::wcout << "Percentile solver computation is diverging!" << std::endl;
                               std::exit(1);
                           },
                           []() {
                               std::wcout << "Percentile solver has stopped responding!" << std::endl;
                               std::exit(1);
                           });

    // setup solver
    std::unique_ptr<ppr::PercentileSolver> solver;
    switch (config.solver_type) {
        case ppr::SolverType::Serial:
            solver = std::make_unique<ppr::PercentileSolverSerial>(
                    std::move(watchdog),
                    config.max_memory,
                    config.load_buffer_size);
            break;
        case ppr::SolverType::Parallel:
            solver = std::make_unique<ppr::PercentileSolverParallel>(
                    std::move(watchdog),
                    config.max_memory,
                    config.load_buffer_size,
                    config.threads);
            break;
        case ppr::SolverType::OpenCL:

            // find OpenCL device by name
            std::optional<cl::Device> device = ppr::get_device(config.device_name);
            if (!device.has_value()) {
                std::wcout << "OpenCL device not available" << std::endl;
                std::exit(1);
            }

            solver = std::make_unique<ppr::PercentileSolverOpenCL>(
                    std::move(watchdog),
                    config.max_memory,
                    config.load_buffer_size,
                    config.states,
                    device.value());
            break;
    }

    // setup file reader
    ppr::FileReader file;
    try {
        file = ppr::FileReader(config.file_path);
    }
    catch (const std::runtime_error &e) {
        std::wcout << "error opening file: " << e.what() << std::endl;
        std::exit(1);
    }

    // run solver
    auto result = solver->solve(file, config.percentile);

    // print results
    if (result.has_value()) {
        auto r = result.value();
        std::wcout << std::hexfloat << r.value << std::dec
                   << " " <<
                   r.first
                   << " " <<
                   r.last
                   << std::endl;
    } else {
        std::wcout << "NaN" << std::endl;
    }
}

/**
 * Run solver for all percentiles (0 - 100) and measure execution times.
 * @param config The solver configuration.
 */
void measure_all(Config config) {
    double avg = 0;
    std::size_t step = config.percentile.get_value();

    for (std::size_t i = 1, count = 1; i <= 100; i += step, count++) {
        config.percentile = ppr::Percentile(i);
        std::wcout << "--- " << i << " percentile ---" << std::endl;

        std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
        solve(config);
        std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

        double time = static_cast<double>(std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count());
        avg = avg + (time - avg) / static_cast<double>(count);

        std::wcout << static_cast<long>(time) << " [ms]" << std::endl;
        std::wcout << std::endl;
    }

    std::wcout << "=== average time ===" << std::endl;
    std::wcout << static_cast<long>(avg) << " [ms]" << std::endl;
}

/**
 * Percentile solver program main function.
 */
int main(int argc, char **argv) {
    Config config = parse_config(argc, argv);

    try {
        if (config.measure_all) {
            // run solver for all percentiles (1 - 100) and measure execution times
            measure_all(config);
        } else if (config.measure_one) {

            // run solver and measure execution time
            std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
            solve(config);
            std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

            double time = static_cast<double>(std::chrono::duration_cast<std::chrono::milliseconds>(
                    end - start).count());
            std::wcout << (long) time << " [ms]" << std::endl;
        } else {
            // just run solver
            solve(config);
        }
    }
    catch (const std::exception &e) {
        std::wcout << "Error occurred: " << e.what() << std::endl;
        return 1;
    }

    return 0;
}