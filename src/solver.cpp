#include "solver.h"

namespace ppr {

    // region Percentile

    Percentile::Percentile() : value(100) {}

    Percentile::Percentile(uint64_t value) : value(value) {
        if (value > 100 || value <= 0) {
            throw std::invalid_argument("Percentile must be between 1 and 100 inclusive.");
        }
    }


    uint64_t Percentile::get_value() const {
        return value;
    }

    // endregion



    // region PercentileSolver

    PercentileSolver::PercentileSolver(Watchdog watchdog,
                                       std::size_t max_memory) noexcept:
            watchdog(std::move(watchdog)),
            max_memory(max_memory) {}


    PercentileSolver::~PercentileSolver() noexcept {
        // force watchdog to stop and join its thread on destruction
        watchdog.stop();
    }


    std::optional<SolverResult> PercentileSolver::solve(FileReader &file, Percentile percentile) {
        // run the watchdog
        watchdog.run();

        double lower = -std::numeric_limits<double>::infinity();
        double pivot = 0.0;
        double upper = std::numeric_limits<double>::infinity();

        bool first_pass = true;

        std::size_t interval_size;
        std::size_t index;

        bool found = false;
        double value;


        // iteratively shrink the interval in which the percentile is located until
        // the number of items in the interval is smaller than max_items

        while (true) {
            PassResult pass_result = partition(file, lower, pivot, upper);

            if (pass_result.eq + pass_result.lt + pass_result.gt == 0) {
                // file has no normal numbers
                return std::nullopt;
            }

            auto[lt, eq, gt, random_lower, random_upper] = pass_result;

            if (first_pass) {
                first_pass = false;
                index = percentile_index(percentile, lt + eq + gt);
            }

            // determine in which interval is the percentile
            if (index < lt) {
                // lower interval
                interval_size = lt;
                upper = pivot;
                pivot = random_lower;
            } else if (index >= lt + eq) {
                // upper interval
                index -= lt + eq;
                interval_size = gt;
                lower = pivot;
                pivot = random_upper;
            } else {
                // exact value of percentile found
                value = pivot;
                found = true;
                break;
            }

            // notify watchdog about new interval size
            watchdog.notify(interval_size);

            if (interval_size <= get_max_interval_size()) {
                // numbers from interval fits to memory of required size
                break;
            }
        }

        if (!found) {
            // find the value on the given percentile from the computed interval
            value = nth_value(file, index, lower, upper, interval_size);
        }

        // find the value's first and last occurrence in the file
        PositionsResult search_result = search(file, value);

        // stop the watchdog
        watchdog.stop();

        return SolverResult{
                value,
                search_result.first,
                search_result.last
        };
    }

    // endregion



    // region functions

    std::size_t percentile_index(Percentile percentile, std::size_t n) {
        // result = ceil(P * N / 100)

        std::size_t a = percentile.get_value() * n;
        std::size_t i = a / 100;

        if (a % 100 == 0) {
            return i - 1;
        } else {
            return i;
        }
    }

    // endregion
}