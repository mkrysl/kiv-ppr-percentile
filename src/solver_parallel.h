#pragma once

#include <tbb/task_arena.h>

#include "solver.h"

namespace ppr {
    /**
     * Parallel percentile solver.
     */
    class PercentileSolverParallel : public PercentileSolver {
        /**
         * TBB task arena.
         */
        tbb::task_arena arena;

        /**
         * The maximum size of the final interval.
         */
        std::size_t max_interval_size;

        /**
         * The size of load buffer for the final interval loading.
         */
        std::size_t load_buffer_size;

        /**
         * The maximum total size of all buffers.
         */
        std::size_t max_buffer_size;

    public:
        /**
         * The size of memory in bytes reserved for runtime.
         */
        static const std::size_t RUNTIME_MEMORY_RESERVE = 50 * MB;

        /**
         * Initialize a parallel percentile solver.
         *
         * Total size of the internal buffer is `max_interval_size` + `load_buffer_size`.
         *
         * @param watchdog The watchdog.
         * @param max_memory The maximum memory used for computation in bytes.
         * @param load_buffer_size The size of the load buffer for final interval loading in bytes.
         * @param threads The number of threads to use.
         */
        PercentileSolverParallel(Watchdog watchdog,
                                 std::size_t max_memory,
                                 std::size_t load_buffer_size,
                                 std::size_t threads);

        std::size_t get_max_interval_size() override;

        PassResult partition(FileReader &file, double lower, double pivot, double upper) override;

        double
        nth_value(FileReader &file, std::size_t index, double lower, double upper, std::size_t interval_size) override;

        PositionsResult search(FileReader &file, double value) override;
    };
}