#include "io.h"

namespace ppr {

    FileReader::FileReader(const std::filesystem::path &file_path)
            : file(std::ifstream()) {

        // open file for binary reading
        file.open(file_path, std::ios::binary);

        if (!file.is_open()) {
            throw std::runtime_error("can not open input file: " + file_path.string());
        }
    }


    void FileReader::reset() {
        file.clear();
        file.seekg(0, std::ios::beg);
    }
}
